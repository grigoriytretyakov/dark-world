import 'phaser';

import { BootLoader } from './BootLoader';
import { Game } from './Game';


const config = {
    type: Phaser.AUTO,
    parent: 'game',
    width: 800,
    height: 600,
    backgroundColor: '#00212b',

    physics: {
        default: 'arcade',
        arcade: {
            gravity: {
                x: 0,
                y: 500
            },
            debug: false,
        }
    },

    scene: [
        BootLoader,
        Game,
    ]
}

new Phaser.Game(config);
