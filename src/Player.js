const VELOCITY_X = 400;

const OFFSET_X = -5;

const  OFFSET_Y = -4;

class Bullet {
    constructor(scene, x, y, angle) {
        this.scene = scene;

        this.sprite = this.scene.physics.add.sprite(x, y, 'bullet');

        this.sprite.setVelocity(400 * Math.cos(angle), 400 * Math.sin(angle));

        this.sprite.body.allowGravity = false;
    }
};

class Player {
    constructor(scene, x, y) {

        this.canonEnabled = false;

        this.RECHARGE = 100;

        this.timeToRecharge = 0;

        this.JUMP_POWER = -400;

        this.isJumping = false;

        this.scene = scene;

        this.sprite = this.scene.physics.add.sprite(x, y, 'player')
            .setCollideWorldBounds(true);


        this.canon = this.scene.add.sprite(x + OFFSET_X, y + OFFSET_Y, 'canon');

        this.turnToRight();
    }

    enableCanon() {
        this.canonEnabled = true;
    }

    disableCanon() {
        this.canonEnabled = false;
        this.timeToRecharge = 0;
    }

    enableJumping() {
        this.isJumping = true;
    }

    disableJumping() {
        this.isJumping = false;
    }

    turnToLeft() {
        this.sprite.flipX = true;
        this.canon.flipX = true;
        this.sprite.setVelocityX(-VELOCITY_X);
    }

    turnToRight() {
        this.sprite.flipX = false;
        this.canon.flipX = false;
        this.sprite.setVelocityX(VELOCITY_X);
    }

    update(delta, mouseX, mouseY) {
        if (this.isJumping && this.sprite.body.touching.down) {
            this.sprite.setVelocityY(this.JUMP_POWER);
        }

        if (this.sprite.flipX) {
            this.canon.x = this.sprite.x - OFFSET_X;
        } else {
            this.canon.x = this.sprite.x + OFFSET_X;
        }
        this.canon.y = this.sprite.y + OFFSET_Y;

        if (this.canonEnabled && this.timeToRecharge <= 0) {
            this.timeToRecharge = this.RECHARGE;
            let angle = Phaser.Math.Angle.Between(
                this.canon.x, this.canon.y, mouseX, mouseY);
            console.log(mouseX, mouseY);
            new Bullet(this.scene, this.canon.x, this.canon.y, angle);
        }

        if (this.timeToRecharge > 0) {
            this.timeToRecharge -= delta;
        }

        //let angle = Phaser.Math.Angle.Between(
        //    this.canon.x, this.canon.y, mouseX, mouseY);
        //this.canon.rotation = angle;

    }
};

export { Player };