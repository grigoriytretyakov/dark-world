class BootLoader extends Phaser.Scene {
    preload() {
        this.load.image('player', 'assets/player.png');
        this.load.image('canon', 'assets/canon.png');
        this.load.image('bullet', 'assets/bullet.png');
        this.load.image('enemy', 'assets/enemy.png');
        this.load.image('brick', 'assets/brick.png');
    }

    create() {
        this.scene.start('Game');
    }

}

export { BootLoader };