const BRICK_PADDING = 10;

class Brick {
    constructor(scene, x, y) {
        this.scene = scene;

        this.sprite = this.scene.physics.add.staticImage(x, y, 'brick');
    }

    getSize() {
        return {
            height: this.sprite.height,
            width: this.sprite.width,
        }
    }
}

export {
    Brick,
    BRICK_PADDING
}
