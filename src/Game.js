import { Player } from './Player.js';
import { Brick, BRICK_PADDING } from './Brick.js';

const WORLD_WIDTH = 1010;
const WORLD_HEIGHT = 800;


class Enemy {
    constructor(scene, x, y) {
        this.scene = scene;

        this.sprite = this.scene.physics.add.sprite(x, y, 'enemy');
        this.sprite.setGravityY(-500);
    }
}


class Game extends Phaser.Scene {
    constructor() {
        super({key: 'Game'});
        this.blockColor = 0x111111;

        this.mouseX = 0;
        this.mouseY = 0;

        console.log('angle:', Phaser.Math.Angle.Between(0, 0, 100, 100));
    }

    create() {
        this.physics.world.setBounds(0, 0, WORLD_WIDTH, WORLD_HEIGHT);
        this.cameras.main.setBounds(0, 0, WORLD_WIDTH, WORLD_HEIGHT);

        this.createLabirint();

        this.createPlayer();
        this.cameras.main.startFollow(this.player.sprite, true);

        this.createEnemies();

        this.createInput();
    }

    createLabirint() {
        this.bricks = [];
        let y = 0;
        while (y < WORLD_HEIGHT) {
            let brickLeft = new Brick(this, 0, y);
            this.bricks.push(brickLeft);
            let brickRight = new Brick(this, WORLD_WIDTH, y);
            this.bricks.push(brickRight);
            y += (brickRight.getSize().height + BRICK_PADDING);
        }

        let x = this.bricks[0].getSize().width + BRICK_PADDING;
        while (x < WORLD_WIDTH - 32) {
            let upperBrick = new Brick(this, x, 0);
            this.bricks.push(upperBrick);
            let bottomBrick = new Brick(this, x, WORLD_HEIGHT);
            this.bricks.push(bottomBrick);
            x += (bottomBrick.getSize().width + BRICK_PADDING);
        }

        x = 100;
        y = 100;
        while (x < WORLD_WIDTH - 100 && y < WORLD_HEIGHT - 100) {
            let brick = new Brick(this, x, y);
            this.bricks.push(brick);
            x += brick.getSize().width + BRICK_PADDING;
            y += brick.getSize().height + BRICK_PADDING;
        }
    }

    createEnemies() {
        this.enemies = [];
        for (let i = 0; i < 10; i++) {
            this.enemies.push(new Enemy(this, 100 + i * 35, WORLD_HEIGHT - 200));
        }
    }

    createPlayer() {
        this.player = new Player(this, 100, WORLD_HEIGHT - 100);
    }

    createInput() {
        this.jump = this.input.keyboard.addKey(
            Phaser.Input.Keyboard.KeyCodes.W);

        this.left = this.input.keyboard.addKey(
            Phaser.Input.Keyboard.KeyCodes.A);

        this.right = this.input.keyboard.addKey(
            Phaser.Input.Keyboard.KeyCodes.D);

        this.input.on('pointerdown', (event) => {
            this.mouseX = event.x;
            this.mouseY = event.y;
            this.player.enableCanon();
        }, this);

        this.input.on('pointerup', (event) => {
            this.player.disableCanon();
        }, this);

        this.input.on('pointermove', (event) => {
            this.mouseX = event.x;
            this.mouseY = event.y;
        }, this);
    }

    update(time, delta) {
        if (this.jump.isDown) {
            this.player.enableJumping();
        } else {
            this.player.disableJumping();
        }

        if (this.right.isDown) {
            this.player.turnToRight();
        } else if (this.left.isDown) {
            this.player.turnToLeft();
        }

        this.physics.world.collide(this.player.sprite, this.bricks.map(brick => brick.sprite));
        this.physics.world.collide(this.player.sprite, this.enemies.map(enemy => enemy.sprite));

        this.player.update(delta, this.mouseX, this.mouseY);

        this.player.sprite.body.postUpdate(); // Fix for https://github.com/photonstorm/phaser/issues/3777
    }
}

export { Game };
